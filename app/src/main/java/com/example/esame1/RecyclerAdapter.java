package com.example.esame1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;
import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.TodoItemHolder> {
    private ArrayList<TaskItem> m_TaskItems;

    public RecyclerAdapter(ArrayList<TaskItem> items) {
        m_TaskItems = items;
    }

    @Override
    public int getItemCount() {
        return m_TaskItems.size();
    }

    @Override
    public RecyclerAdapter.TodoItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_row, parent, false);
        return new TodoItemHolder(parent, inflatedView);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.TodoItemHolder holder, int position) {
        TaskItem taskItem = m_TaskItems.get(position);
        holder.bindTodoItem(taskItem);
    }

    public static class TodoItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ViewGroup m_Parent = null;
        private TaskItem m_TaskItem = null;
        private ImageView m_ItemQrCode = null;
        private ImageView m_ItemInfo = null;
        private TextView m_ItemTitle = null;
        private LinearLayout m_ItemUsageIconsContainer = null;

        public TodoItemHolder(ViewGroup parent, View itemView) {
            super(itemView);
            m_Parent = parent;
            m_ItemQrCode = (ImageView) itemView.findViewById(R.id.item_qr_code);
            m_ItemInfo = (ImageView) itemView.findViewById(R.id.item_info);
            m_ItemTitle = (TextView) itemView.findViewById(R.id.item_title);
            m_ItemUsageIconsContainer = (LinearLayout) itemView.findViewById(R.id.item_usage_icons);

            m_ItemQrCode.setOnClickListener(this);
            m_ItemInfo.setOnClickListener(this);
        }

        public void bindTodoItem(TaskItem item) {
            m_TaskItem = item;

            try {
                m_ItemTitle.setText(item.getTitle());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                m_ItemUsageIconsContainer.removeAllViewsInLayout();
                m_ItemUsageIconsContainer.setWeightSum(12f);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.MATCH_PARENT, 1f);

                if (m_TaskItem.hasWifiSettings())
                {
                    ImageView myImage = new ImageView(m_Parent.getContext());
                    myImage.setImageResource(R.drawable.ic_wifi);
                    myImage.setColorFilter(m_Parent.getContext().getResources().getColor(R.color.colorWifiBackground));
                    myImage.setLayoutParams(lp);
                    m_ItemUsageIconsContainer.addView(myImage);
                }
                if (m_TaskItem.hasBluetoothSettings())
                {
                    ImageView myImage = new ImageView(m_Parent.getContext());
                    myImage.setImageResource(R.drawable.ic_bluetooth);
                    myImage.setColorFilter(m_Parent.getContext().getResources().getColor(R.color.colorBluetoothBackground));
                    myImage.setLayoutParams(lp);
                    m_ItemUsageIconsContainer.addView(myImage);
                }
                if (m_TaskItem.hasGpsSettings())
                {
                    ImageView myImage = new ImageView(m_Parent.getContext());
                    myImage.setImageResource(R.drawable.ic_gps);
                    myImage.setColorFilter(m_Parent.getContext().getResources().getColor(R.color.colorGpsBackground));
                    myImage.setLayoutParams(lp);
                    m_ItemUsageIconsContainer.addView(myImage);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            m_ItemQrCode.post(new Runnable() {
                public void run() {
                    Bitmap qr = QrCodeWriter.generateQr(m_TaskItem.getDirtyTask());
                    if (qr != null) {
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        lp.setMargins(0,0,0,0);
                        m_ItemQrCode.setLayoutParams(lp);
                        m_ItemQrCode.setImageBitmap(qr);
                    }
                }
            });
        }

        public void onQrClickShowPopup() {
            View inflatedView = LayoutInflater.from(m_Parent.getContext()).inflate(R.layout.qr_popup, m_Parent, false);
            final PopupWindow popupWindow = new PopupWindow(inflatedView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, true);
            View popupView = popupWindow.getContentView();

            final ImageView qrCodeShare = (ImageView) popupView.findViewById(R.id.qr_code_share);

            qrCodeShare.post(new Runnable() {
                public void run() {
                    Bitmap qr = QrCodeWriter.generateLargeQr(m_TaskItem.getDirtyTask());
                    if (qr != null) {
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        lp.setMargins(0,0,0,0);
                        qrCodeShare.setLayoutParams(lp);
                        qrCodeShare.setImageBitmap(qr);
                    }
                }
            });

            LinearLayout shareButton = (LinearLayout) popupView.findViewById(R.id.share_container);
            shareButton.setOnClickListener(this);

            popupWindow.showAtLocation(m_Parent, Gravity.CENTER, 0, 0);
            final Animation animFadeIn = AnimationUtils.loadAnimation(m_Parent.getContext(),R.anim.fade_in);
            final LinearLayout qrContainer = (LinearLayout) popupView.findViewById(R.id.qr_container);
            qrContainer.startAnimation(animFadeIn);

            popupView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    popupWindow.dismiss();
                    return true;
                }
            });
        }

        private void OnInfoClickShowSummary() {
            Intent intent = new Intent(m_Parent.getContext(), InfoTaskActivity.class);
            intent.putExtra("TASK", m_TaskItem.getCleanTask());
            m_Parent.getContext().startActivity(intent);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.item_qr_code:
                    onQrClickShowPopup();
                    break;
                case R.id.share_container:
                    onShareQrCode();
                    break;
                case R.id.item_info:
                    OnInfoClickShowSummary();
                    break;
                default:
                    Log.d("RecyclerView", "CLICK!");
            }
        }

        private void onShareQrCode() {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        Bitmap qr = QrCodeWriter.generateLargeQrToShare(m_TaskItem.getDirtyTask());

                        if (qr != null) {
                            Uri uri = saveImage(qr);
                            if (uri != null) {
                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.putExtra(Intent.EXTRA_STREAM, uri);
                                intent.setType("image/png");
                                m_Parent.getContext().startActivity(Intent.createChooser(intent, "Share"));
                            }
                        }
                    }
                    catch (Exception e) {
                        Toast.makeText(m_Parent.getContext(), m_Parent.getContext().getText(R.string.error_occurred_retry), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
            }).run();
        }

        /**
         * Saves the image as PNG to the app's cache directory.
         * @param image Bitmap to save.
         * @return Uri of the saved file or null
         */
        private Uri saveImage(Bitmap image) {
            File imagesFolder = new File(m_Parent.getContext().getCacheDir(), "images");
            Uri uri = null;
            try {
                imagesFolder.mkdirs();
                File file = new File(imagesFolder, "shared_image.png");

                FileOutputStream stream = new FileOutputStream(file);
                image.compress(Bitmap.CompressFormat.PNG, 100, stream);
                stream.flush();
                stream.close();
                uri = FileProvider.getUriForFile(m_Parent.getContext(), m_Parent.getContext().getPackageName() + ".fileprovider", file);
            } catch (IOException e) {
                Log.d(TAG, "IOException while trying to write file for sharing: " + e.getMessage());
            }
            return uri;
        }
    }
}
