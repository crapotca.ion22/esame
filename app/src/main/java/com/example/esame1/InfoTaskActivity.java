package com.example.esame1;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import org.json.JSONException;
import org.json.JSONObject;

public class InfoTaskActivity extends AppCompatActivity {
    JSONObject m_jsonSettings = new JSONObject();
    Boolean m_isConfirm = false;
    Boolean m_isAfterScan = false;
    Boolean m_isExecute = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_task);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.task_info);

        Intent intent = getIntent();
        m_isConfirm = intent.hasExtra("CONFIRM");
        m_isAfterScan = intent.hasExtra("AFTER_SCAN");
        m_isExecute = intent.hasExtra("EXECUTE");

        if (intent.hasExtra("TASK")) {
            try {
                String strTask = intent.getStringExtra("TASK");
                m_jsonSettings = new JSONObject(strTask);
                Fragment fr = new InfoTaskFragment(m_jsonSettings, m_isExecute);
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();

                fragmentTransaction.add(R.id.summaryFragmentContainer, fr);
                fragmentTransaction.commit();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (!m_isConfirm) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (m_isConfirm)
            getMenuInflater().inflate(R.menu.menu_confirm, menu);
        if (m_isAfterScan)
            getMenuInflater().inflate(R.menu.menu_after_scan, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_undo:
                onActionUndo();
                break;
            case R.id.action_confirm:
                onActionConfirm();
                break;
            case R.id.action_add:
                onActionAdd();
                break;
            case R.id.action_start:
                onActionStart();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void onActionUndo() {
        onBackPressed();
        finish();
    }

    private void onActionConfirm() {
        Intent intent = new Intent();
        intent.putExtra("TASK", m_jsonSettings.toString());
        setResult(RESULT_OK, intent);
        finish();
    }

    private void onActionAdd() {
        Intent intent = new Intent();
        intent.putExtra("ADD", true);
        intent.putExtra("TASK", m_jsonSettings.toString());
        setResult(RESULT_OK, intent);
        finish();
    }

    private void onActionStart() {
        Intent intent = new Intent();
        intent.putExtra("START", true);
        intent.putExtra("TASK", m_jsonSettings.toString());
        setResult(RESULT_OK, intent);
        finish();
    }

}
