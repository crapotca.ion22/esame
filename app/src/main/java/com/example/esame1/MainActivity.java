package com.example.esame1;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.notbytes.barcode_reader.BarcodeReaderActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int BARCODE_READER_ACTIVITY_REQUEST = 1208;
    private static final int EDIT_ACTIVITY_REQUEST = 1209;
    private static final int AFTER_SCAN_ACTIVITY_REQUEST = 1210;
    private static final int EXECUTE_ACTIVITY_REQUEST = 1211;
    private static final String TAG = "MainActivity";

    private RecyclerView m_recyclerView;
    private RecyclerView.Adapter m_adapter;
    private RecyclerView.LayoutManager m_layoutManager;
    private ArrayList<TaskItem> m_taskItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        handleFabGestures(fab);

        m_recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        m_layoutManager = new LinearLayoutManager(this);
        m_recyclerView.setLayoutManager(m_layoutManager);
        m_taskItems = new ArrayList<>();
        m_adapter = new RecyclerAdapter(m_taskItems);
        m_recyclerView.setAdapter(m_adapter);
        setSwiper(this);

        m_recyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                showPopupHint();
            }
        }, 500);
    }

    private void setSwiper(final Context context) {
        SwipeHelper swipeHelper = new SwipeHelper(context, m_recyclerView) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> leftUnderlayButtons, List<UnderlayButton> rightUnderlayButtons) {

                leftUnderlayButtons.add(new SwipeHelper.UnderlayButton(
                        context,
                        R.drawable.ic_start,
                        R.color.colorSwipeStartButtonBackground,
                        R.color.colorSwipeStartButtonText,
                        new SwipeHelper.UnderlayButtonClickListener() {
                            @Override
                            public void onClick(int pos) {
                                TaskItem taskItem = m_taskItems.get(pos);
                                onStartTask(taskItem);
                            }
                        }
                ));
                leftUnderlayButtons.add(new SwipeHelper.UnderlayButton(
                        context,
                        R.drawable.ic_pencil,
                        R.color.colorSwipeEditButtonBackground,
                        R.color.colorSwipeEditButtonText,
                        new SwipeHelper.UnderlayButtonClickListener() {
                            @Override
                            public void onClick(int pos) {
                                TaskItem taskItem = m_taskItems.get(pos);
                                onEditTask(taskItem);
                            }
                        }
                ));

                rightUnderlayButtons.add(new SwipeHelper.UnderlayButton(
                    context,
                    R.drawable.ic_delete,
                    R.color.colorSwipeDeleteButtonBackground,
                    R.color.colorSwipeDeleteButtonText,
                    new SwipeHelper.UnderlayButtonClickListener() {
                        @Override
                        public void onClick(final int pos) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle(R.string.warning).setMessage(R.string.ask_sure_delete).setCancelable(false)
                            .setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    TaskItem item = m_taskItems.remove(pos);
                                    DatabaseHelper.getInstance(context).deleteItem(item);
                                    m_recyclerView.getAdapter().notifyItemRemoved(pos);
                                }
                            })
                            .setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });

                        AlertDialog alert = builder.create();
                        alert.show();
                        }
                    }
                ));
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v(TAG, "onResume()");

        m_taskItems.clear();
        m_taskItems.addAll(DatabaseHelper.getInstance(this).getAllItems());
        m_adapter.notifyDataSetChanged();
    }

    private void addNewItem(String task){
        TaskItem newTask = new TaskItem(this, task, false);
        if (!DatabaseHelper.getInstance(this).itemAlreadyExists(newTask)) {
            m_taskItems.add(newTask);
            m_adapter.notifyItemInserted(m_taskItems.size());
            long idx = DatabaseHelper.getInstance(this).insertItem(newTask);
            newTask.setId(idx);
        }
        else
        {
            Toast.makeText(this, R.string.task_already_exists, Toast.LENGTH_SHORT).show();
        }
    }

    private void updateTask(long taskId, String task) {
        TaskItem taskItem = DatabaseHelper.getInstance(this).getItem(taskId);
        if (taskItem != null)
        {
            taskItem.setTaskFromClean(task);
            DatabaseHelper.getInstance(this).updateItem(taskId, taskItem);
        }
    }

    private void handleFabGestures(final FloatingActionButton fab) {
        GestureHelper fabGestures = new GestureHelper(this) {
            @Override
            public void onClick() {
                Log.i("FAB CLICK", "Fab was clicked");
                launchBarCodeActivity();
            }

            @Override
            public void onSwipeLeft() {
                CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
                Log.i("FAB SWAP", "Fab moved from right to left");
                lp.gravity = Gravity.BOTTOM | Gravity.START;
                fab.setLayoutParams(lp);
            }

            @Override
            public void onSwipeRight() {
                CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
                Log.i("FAB SWAP", "Fab moved from left to right");
                lp.gravity = Gravity.BOTTOM | Gravity.END;
                fab.setLayoutParams(lp);
            }
        };
        fab.setOnTouchListener(fabGestures);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_new_task:
                onActionNewTask();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void onActionNewTask() {
        Intent intent = new Intent(this, EditTaskActivity.class);
        startActivityForResult(intent, EDIT_ACTIVITY_REQUEST);
    }

    private void onEditTask(TaskItem taskItem) {
        Intent intent = new Intent(this, EditTaskActivity.class);
        intent.putExtra("TASK_ID", taskItem.getId());
        intent.putExtra("TASK", taskItem.getCleanTask());
        startActivityForResult(intent, EDIT_ACTIVITY_REQUEST);
    }

    private void launchBarCodeActivity() {
        Intent launchIntent = BarcodeReaderActivity.getLaunchIntent(this, true, false);
        startActivityForResult(launchIntent, BARCODE_READER_ACTIVITY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case BARCODE_READER_ACTIVITY_REQUEST:
                    if (data != null) {
                        final Vibrator vibe = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

                        String barcodeContent = data.getStringExtra(BarcodeReaderActivity.KEY_CAPTURED_RAW_BARCODE);
                        TaskItem scannedTask = new TaskItem(this, barcodeContent, true);
                        if (scannedTask.isValid()) {
                            vibe.vibrate(100);

                            Intent intent = new Intent(this, InfoTaskActivity.class);
                            intent.putExtra("AFTER_SCAN", true);
                            intent.putExtra("TASK", scannedTask.getCleanTask());
                            startActivityForResult(intent, AFTER_SCAN_ACTIVITY_REQUEST);
                        } else {
                            vibe.vibrate(500);

                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setTitle(R.string.error).setMessage(R.string.wrong_scan).setCancelable(false)
                                    .setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            launchBarCodeActivity();
                                        }
                                    })
                                    .setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                        }
                                    });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    }
                    break;
                case EDIT_ACTIVITY_REQUEST:
                    if (data.hasExtra("TASK_ID"))
                        updateTask(data.getLongExtra("TASK_ID", -1), data.getStringExtra("TASK"));
                    else
                        addNewItem(data.getStringExtra("TASK"));
                    break;

                case AFTER_SCAN_ACTIVITY_REQUEST:
                    if (data.hasExtra("ADD"))
                        addNewItem(data.getStringExtra("TASK"));
                    if (data.hasExtra("START"))
                        onStartTask(new TaskItem(this, data.getStringExtra("TASK"), false));
                    break;
            }
        }
    }

    private void onStartTask(TaskItem taskItem) {
        Intent intent = new Intent(this, InfoTaskActivity.class);
        intent.putExtra("EXECUTE", true);
        intent.putExtra("TASK", taskItem.getCleanTask());
        startActivityForResult(intent, EXECUTE_ACTIVITY_REQUEST);
    }

    private void showPopupHint() {
        if (!getSharedPreferences("MyPrefsFile1", MODE_PRIVATE).getBoolean("showPopupHint", false)) {
            View inflatedView = LayoutInflater.from(this).inflate(R.layout.hint_popup, null);
            final PopupWindow popupWindow = new PopupWindow(inflatedView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, true);
            final View popupView = popupWindow.getContentView();

            popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.CENTER, 0, 0);
            final Animation animFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
            final LinearLayout hintContainer = (LinearLayout) popupView.findViewById(R.id.hint_container);
            hintContainer.startAnimation(animFadeIn);

            TextView leftSwipeText = (TextView) popupView.findViewById(R.id.swipe_left_text);
            leftSwipeText.setText(Html.fromHtml(getResources().getString(R.string.swipe_left_feature)));
            TextView rightSwipeText = (TextView) popupView.findViewById(R.id.swipe_right_text);
            rightSwipeText.setText(Html.fromHtml(getResources().getString(R.string.swipe_right_feature)));

            popupView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    CheckBox checkDontShow = (CheckBox) popupView.findViewById(R.id.checkDontShowAgain);
                    if (checkDontShow.isChecked())
                        getSharedPreferences("MyPrefsFile1", MODE_PRIVATE).edit().putBoolean("showPopupHint", true).apply();
                    popupWindow.dismiss();
                    return true;
                }
            });
        }
    }
}
