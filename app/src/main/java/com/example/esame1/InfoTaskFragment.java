package com.example.esame1;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.diegodobelo.expandingview.ExpandingItem;
import com.diegodobelo.expandingview.ExpandingList;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.WIFI_SERVICE;

public class InfoTaskFragment extends Fragment {
    boolean m_isExecute;
    JSONObject m_jsonSettings;
    ExpandingList m_taskSettings;

    public InfoTaskFragment(JSONObject jsonSettings, boolean isExecute) {
        m_jsonSettings = jsonSettings;
        m_isExecute = isExecute;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_info_task, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        m_taskSettings = (ExpandingList) getView().findViewById(R.id.task_settings);

        initializeTitle();
        initializeWifiSettings();
        initializeBluetoothSettings();
        initializeGpsSettings();
    }

    private void initializeTitle() {
        String title = "";
        if (m_jsonSettings.has("TITLE")) {
            try {
                title = m_jsonSettings.getString("TITLE");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        TextView textViewTitle = (TextView) getView().findViewById(R.id.textViewTitle);
        textViewTitle.setText(title);
    }

    private void initializeWifiSettings() {
        if (!m_jsonSettings.has("WIFI"))
            return;

        Boolean showCollapsed = true;
        JSONObject jsonWifi = new JSONObject();
        try {
            jsonWifi = m_jsonSettings.getJSONObject("WIFI");
            showCollapsed = false;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ExpandingItem wifiSettingsItem = m_taskSettings.createNewItem(getString(R.string.wifi), R.drawable.ic_wifi, R.color.colorWifiBackground, R.color.colorWifiIcon, showCollapsed);

        if (jsonWifi.has("TURN")) {
            try {
                String turnState = jsonWifi.getString("TURN");
                if (turnState.compareTo("OFF") == 0) {
                    View subItem = addSubItem(wifiSettingsItem, R.string.turn_off, R.color.colorWifiBackground);
                    if (m_isExecute)
                        turnWifi(subItem, false);
                }
                if (turnState.compareTo("ON") == 0) {
                    View subItem = addSubItem(wifiSettingsItem, R.string.turn_on, R.color.colorWifiBackground);
                    if (m_isExecute)
                        turnWifi(subItem, true);
                }

                if (jsonWifi.has("CONNECT"))
                {
                    JSONObject jsonConnect = jsonWifi.getJSONObject("CONNECT");
                    String ssid = jsonConnect.getString("SSID");

                    String type = jsonConnect.getString("TYPE");
                    int typeRes = 0;
                    if (type.compareTo("OPEN") == 0)
                        typeRes = R.string.open_network;
                    if (type.compareTo("WEP") == 0)
                        typeRes = R.string.wep_network;
                    if (type.compareTo("WPA") == 0)
                        typeRes = R.string.wpa_network;

                    String connectToNetwork = String.format(getResources().getString(R.string.connect_to_network), ssid, getResources().getString(typeRes));
                    View subItem = addSubItem(wifiSettingsItem, connectToNetwork, R.color.colorWifiBackground);

                    if (m_isExecute) {
                        String password = "";
                        if (jsonConnect.has("PASSWORD"))
                            password = jsonConnect.getString("PASSWORD");
                        connectToWifi(subItem, type, ssid, password);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void initializeBluetoothSettings() {
        if (!m_jsonSettings.has("BLUETOOTH"))
            return;

        Boolean showCollapsed = true;
        JSONObject jsonBluetooth = new JSONObject();
        try {
            jsonBluetooth = m_jsonSettings.getJSONObject("BLUETOOTH");
            showCollapsed = false;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ExpandingItem bluetoothSettingsItem = m_taskSettings.createNewItem(getString(R.string.bluetooth), R.drawable.ic_bluetooth, R.color.colorBluetoothBackground, R.color.colorBluetoothIcon, showCollapsed);

        if (jsonBluetooth.has("TURN")) {
            try {
                String turnState = jsonBluetooth.getString("TURN");
                if (turnState.compareTo("OFF") == 0) {
                    View subItem = addSubItem(bluetoothSettingsItem, R.string.turn_off, R.color.colorBluetoothBackground);
                    if (m_isExecute)
                        turnBluetooth(subItem, false);
                }
                if (turnState.compareTo("ON") == 0) {
                    View subItem = addSubItem(bluetoothSettingsItem, R.string.turn_on, R.color.colorBluetoothBackground);
                    if (m_isExecute)
                        turnBluetooth(subItem, true);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void initializeGpsSettings() {
        if (!m_jsonSettings.has("GPS"))
            return;

        Boolean showCollapsed = true;
        JSONObject jsonGps = new JSONObject();
        try {
            jsonGps = m_jsonSettings.getJSONObject("GPS");
            showCollapsed = false;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ExpandingItem gpsSettingsItem = m_taskSettings.createNewItem(getString(R.string.gps), R.drawable.ic_gps, R.color.colorGpsBackground, R.color.colorGpsIcon, showCollapsed);

        if (jsonGps.has("TURN")) {
            try {
                String turnState = jsonGps.getString("TURN");
                if (turnState.compareTo("OFF") == 0) {
                    View subItem = addSubItem(gpsSettingsItem, R.string.turn_off, R.color.colorGpsBackground);
                    if (m_isExecute)
                        turnGps(subItem, false);
                }
                if (turnState.compareTo("ON") == 0) {
                    View subItem = addSubItem(gpsSettingsItem, R.string.turn_on, R.color.colorGpsBackground);
                    if (m_isExecute)
                        turnGps(subItem, true);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private View addSubItem(ExpandingItem parent, int stringRes, int dotColorRes) {
        return addSubItem(parent, getResources().getString(stringRes), dotColorRes);
    }

    private View addSubItem(ExpandingItem parent, String content, int dotColorRes) {
        View subItem = parent.createSubItem(R.layout.task_summary_sub_item);
        ImageView dot = (ImageView) subItem.findViewById(R.id.subItemDot);
        dot.setColorFilter(getResources().getColor(dotColorRes));
        TextView text = (TextView) subItem.findViewById(R.id.subItemText);
        text.setText(content);
        return subItem;
    }

    private void turnWifi(View subItem, boolean enable) {
        boolean turnStatus = turnWifiCore(enable);
        ImageView dot = (ImageView) subItem.findViewById(R.id.subItemDot);
        if (turnStatus)
            dot.setImageResource(R.drawable.ic_check);
        else
            dot.setImageResource(R.drawable.ic_cross);
    }

    private void connectToWifi(View subItem, String type, String ssid, String password) {
        boolean turnStatus = connectToWifiCore(type, ssid, password);
        ImageView dot = (ImageView) subItem.findViewById(R.id.subItemDot);
        if (turnStatus)
            dot.setImageResource(R.drawable.ic_check);
        else
            dot.setImageResource(R.drawable.ic_cross);
    }

    private boolean connectToWifiCore(String type, String ssid, String password) {
        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", ssid);

        if (type.compareTo("WPA") == 0)
            wifiConfig.preSharedKey = String.format("\"%s\"", password);
        if (type.compareTo("WEP") == 0) {
            wifiConfig.wepKeys[0] = "\"" + password + "\"";
            wifiConfig.wepTxKeyIndex = 0;
            wifiConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            wifiConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        }
        if (type.compareTo("OPEN") == 0)
            wifiConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

        WifiManager wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(WIFI_SERVICE);
        int netId = wifiManager.addNetwork(wifiConfig);
        wifiManager.disconnect();
        wifiManager.enableNetwork(netId, true);
        wifiManager.reconnect();

        return true;
    }

    private boolean turnWifiCore(boolean enable) {
        WifiManager wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(WIFI_SERVICE);
        boolean isEnabled = wifiManager.isWifiEnabled();
        if (enable && !isEnabled) {
            return wifiManager.setWifiEnabled(true);
        }
        else if(!enable && isEnabled) {
            return wifiManager.setWifiEnabled(false);
        }
        return false;
    }

    private void turnBluetooth(View subItem, boolean enable) {
        boolean turnStatus = turnBluetoothCore(enable);
        ImageView dot = (ImageView) subItem.findViewById(R.id.subItemDot);
        if (turnStatus)
            dot.setImageResource(R.drawable.ic_check);
        else
            dot.setImageResource(R.drawable.ic_cross);
    }

    private boolean turnBluetoothCore(boolean enable) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        boolean isEnabled = bluetoothAdapter.isEnabled();
        if (enable && !isEnabled) {
            return bluetoothAdapter.enable();
        }
        else if(!enable && isEnabled) {
            return bluetoothAdapter.disable();
        }
        return false;
    }

    private void turnGps(View subItem, boolean enable) {
        boolean turnStatus = turnGpsCore(enable);
        ImageView dot = (ImageView) subItem.findViewById(R.id.subItemDot);
        if (turnStatus)
            dot.setImageResource(R.drawable.ic_check);
        else
            dot.setImageResource(R.drawable.ic_cross);
    }

    private boolean turnGpsCore(boolean enable) {
        Toast.makeText(getActivity().getApplicationContext(), R.string.disabled_because_not_programmatical, Toast.LENGTH_SHORT).show();
        return false;
    }

}
