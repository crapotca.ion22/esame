package com.example.esame1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.LinkedList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static DatabaseHelper sInstance;
    private Context m_context = null;

    private static final String TAG = "DatabaseHelper";
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "tasks.db";
    // Books table name
    private static final String TABLE_NAME = "tasks";
    // Books Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_DIRTY_TASK = "dirty_task";
    private static final String KEY_CLEAN_TASK = "clean_task";

    private DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        m_context = context;
    }

    public static synchronized DatabaseHelper getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        if (sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "
                + TABLE_NAME + " ("
                + KEY_ID + " integer primary key autoincrement, "
                + KEY_CLEAN_TASK + " TEXT not null,"
                + KEY_DIRTY_TASK + " TEXT not null"
                + ");");
        Log.d(TAG, "onCreate(): create table");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        // create fresh books table
        this.onCreate(db);
        Log.d(TAG, "onUpgrade(): created fresh table");
    }

    public List<TaskItem> getAllItems() {
        List<TaskItem> items = new LinkedList<TaskItem>();
        String query = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        TaskItem task = null;
        if (cursor.moveToFirst()) {
            do {
                task = new TaskItem(sInstance.m_context);
                task.setId(Integer.parseInt(cursor.getString(0)));
                task.setTaskFromClean(cursor.getString(1));
                items.add(task);
            } while (cursor.moveToNext());
        }
        db.close();
        Log.d(TAG, "getAllItems(): "+ items.toString());
        return items;
    }

    public TaskItem getItem(long item_id) {
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_ID + " = ?";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { String.valueOf(item_id) });
        TaskItem task = null;
        if (cursor.moveToFirst()) {
            task = new TaskItem(sInstance.m_context);
            task.setId(Integer.parseInt(cursor.getString(0)));
            task.setTaskFromClean(cursor.getString(1));
        }
        db.close();
        Log.d(TAG, "getItem(" + item_id + "): "+ task.toString());
        return task;
    }

    public long updateItem(long item_id, TaskItem item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CLEAN_TASK, item.getCleanTask());
        values.put(KEY_DIRTY_TASK, item.getDirtyTask());
        long id = db.update(TABLE_NAME, values, KEY_ID+" = ?", new String[] { String.valueOf(item_id) });
        db.close();
        Log.d(TAG, "updateItem("+item_id+") " + item.toString());
        return id;
    }

    public void deleteItem(TaskItem item) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, KEY_ID+" = ?", new String[] { String.valueOf(item.getId()) });
        db.close();
        Log.d(TAG, "deleted item "+item.toString());
    }

    public Boolean itemAlreadyExists(TaskItem item) {
        Boolean exists = false;
        String query = "SELECT COUNT(*) FROM " + TABLE_NAME + " WHERE " + KEY_CLEAN_TASK + " = ?";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[] { item.getCleanTask() });
        if (cursor.moveToFirst()) {
            if (cursor.getInt(0) > 0)
                exists = true;
        }
        db.close();
        Log.d(TAG, "itemAlreadyExists(): " + exists.toString() + " " + item.toString());
        return exists;
    }

    public long insertItem(TaskItem item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CLEAN_TASK, item.getCleanTask());
        values.put(KEY_DIRTY_TASK, item.getDirtyTask());
        long id = db.insert(TABLE_NAME, null, values);
        db.close();
        Log.d(TAG, "insertItem("+id+") " + item.toString());
        return id;
    }
}
