package com.example.esame1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.diegodobelo.expandingview.ExpandingItem;
import com.diegodobelo.expandingview.ExpandingList;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

public class EditTaskActivity extends AppCompatActivity {
    private static final int CONFIRM_TASK_ACTIVITY_REQUEST = 1208;
    long m_taskId = -1;
    ExpandingList m_taskSettings;
    View m_wifiSettings;
    View m_wifiSettings2;
    View m_bluetoothSettings;
    View m_gpsSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        JSONObject jsonSettings = new JSONObject();
        if (intent.hasExtra("TASK_ID"))
        {
            m_taskId = intent.getLongExtra("TASK_ID", -1);
            if (intent.hasExtra("TASK")) {
                try {
                    jsonSettings = new JSONObject(intent.getStringExtra("TASK"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        if (m_taskId != -1)
            getSupportActionBar().setTitle(R.string.edit_task);
        else
            getSupportActionBar().setTitle(R.string.new_task);

        m_taskSettings = (ExpandingList) findViewById(R.id.task_settings);

        initializeTitle(jsonSettings);
        initializeWifiSettings(jsonSettings);
        initializeBluetoothSettings(jsonSettings);
        initializeGpsSettings(jsonSettings);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        if (event.getAction() == MotionEvent.ACTION_DOWN) {

            /**
             * It gets into the above IF-BLOCK if anywhere the screen is touched.
             */

            View v = getCurrentFocus();
            if ( v instanceof EditText) {


                /**
                 * Now, it gets into the above IF-BLOCK if an EditText is already in focus, and you tap somewhere else
                 * to take the focus away from that particular EditText. It could have 2 cases after tapping:
                 * 1. No EditText has focus
                 * 2. Focus is just shifted to the other EditText
                 */

                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    private void initializeTitle(JSONObject jsonSettings) {
        String title = "";
        if (jsonSettings.has("TITLE")) {
            try {
                title = jsonSettings.getString("TITLE");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        EditText editTitle = (EditText) findViewById(R.id.editTextTitle);
        editTitle.setText(title);
    }

    private void initializeWifiSettings(JSONObject jsonSettings) {
        Boolean showCollapsed = true;
        JSONObject jsonWifi = new JSONObject();
        if (jsonSettings.has("WIFI")) {
            try {
                jsonWifi = jsonSettings.getJSONObject("WIFI");
                showCollapsed = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ExpandingItem wifiSettingsItem = m_taskSettings.createNewItem(getString(R.string.wifi), R.drawable.ic_wifi, R.color.colorWifiBackground, R.color.colorWifiIcon, showCollapsed);
        m_wifiSettings = wifiSettingsItem.createSubItem(R.layout.wifi_settings);
        View.OnClickListener wifiClickListener = new View.OnClickListener() {
            public void onClick(View v) {
                onWifiSettingsClick(v);
            }
        };

        CheckBox turnOff = (CheckBox) m_wifiSettings.findViewById(R.id.checkBoxOff);
        CheckBox turnOn = (CheckBox) m_wifiSettings.findViewById(R.id.checkBoxOn);
        turnOff.setOnClickListener(wifiClickListener);
        turnOn.setOnClickListener(wifiClickListener);

        String turnState = "";
        if (jsonWifi.has("TURN")) {
            try {
                turnState = jsonWifi.getString("TURN");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        turnOff.setChecked(turnState.compareTo("OFF") == 0);
        turnOn.setChecked(turnState.compareTo("ON") == 0);

        m_wifiSettings2 = wifiSettingsItem.createSubItem(R.layout.wifi_settings2);

        EditText editSsid = (EditText) m_wifiSettings2.findViewById(R.id.editSSID);
        EditText editPassword = (EditText) m_wifiSettings2.findViewById(R.id.editPassword);

        CheckBox connect = (CheckBox) m_wifiSettings2.findViewById(R.id.checkConnect);
        connect.setOnClickListener(wifiClickListener);

        RadioButton radioOpen = (RadioButton) m_wifiSettings2.findViewById(R.id.radioOpen);
        radioOpen.setOnClickListener(wifiClickListener);
        RadioButton radioWep = (RadioButton) m_wifiSettings2.findViewById(R.id.radioWep);
        radioWep.setOnClickListener(wifiClickListener);
        RadioButton radioWpa = (RadioButton) m_wifiSettings2.findViewById(R.id.radioWpa);
        radioWpa.setOnClickListener(wifiClickListener);

        String ssid = "";
        String password = "";
        if (jsonWifi.has("CONNECT")) {
            try {
                JSONObject jsonConnect = jsonWifi.getJSONObject("CONNECT");
                ssid = jsonConnect.getString("SSID");

                if (jsonConnect.has("PASSWORD"))
                    password = jsonConnect.getString("PASSWORD");

                String type = jsonConnect.getString("TYPE");
                if (type.compareTo("OPEN") == 0)
                    radioOpen.setChecked(true);
                if (type.compareTo("WEP") == 0)
                    radioWep.setChecked(true);
                if (type.compareTo("WPA") == 0)
                    radioWpa.setChecked(true);

                connect.setChecked(true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        editSsid.setText(ssid);
        editPassword.setText(password);

        onWifiSettingsClick(connect);

        if (radioOpen.isChecked()) {
            TextInputLayout layoutPassword = (TextInputLayout) m_wifiSettings2.findViewById(R.id.layoutPassword);
            layoutPassword.setVisibility(View.GONE);
        }
    }

    private void initializeBluetoothSettings(JSONObject jsonSettings) {
        Boolean showCollapsed = true;
        JSONObject jsonBluetooth = new JSONObject();
        if (jsonSettings.has("BLUETOOTH")) {
            try {
                jsonBluetooth = jsonSettings.getJSONObject("BLUETOOTH");
                showCollapsed = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ExpandingItem bluetoothSettingsItem = m_taskSettings.createNewItem(getString(R.string.bluetooth), R.drawable.ic_bluetooth, R.color.colorBluetoothBackground, R.color.colorBluetoothIcon, showCollapsed);
        m_bluetoothSettings = bluetoothSettingsItem.createSubItem(R.layout.bluetooth_settings);
        View.OnClickListener bluetoothClickListener = new View.OnClickListener() {
            public void onClick(View v) {
                onBluetoothSettingsClick(v);
            }
        };

        CheckBox turnOff = (CheckBox) m_bluetoothSettings.findViewById(R.id.checkBoxOff);
        CheckBox turnOn = (CheckBox) m_bluetoothSettings.findViewById(R.id.checkBoxOn);
        turnOff.setOnClickListener(bluetoothClickListener);
        turnOn.setOnClickListener(bluetoothClickListener);

        String turnState = "";
        if (jsonBluetooth.has("TURN")) {
            try {
                turnState = jsonBluetooth.getString("TURN");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        turnOff.setChecked(turnState.compareTo("OFF") == 0);
        turnOn.setChecked(turnState.compareTo("ON") == 0);
    }

    private void initializeGpsSettings(JSONObject jsonSettings) {
        Boolean showCollapsed = true;
        JSONObject jsonGps = new JSONObject();
        if (jsonSettings.has("GPS")) {
            try {
                jsonGps = jsonSettings.getJSONObject("GPS");
                showCollapsed = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ExpandingItem gpsSettingsItem = m_taskSettings.createNewItem(getString(R.string.gps), R.drawable.ic_gps, R.color.colorGpsBackground, R.color.colorGpsIcon, showCollapsed);
        m_gpsSettings = gpsSettingsItem.createSubItem(R.layout.gps_settings);

        View.OnClickListener gpsClickListener = new View.OnClickListener() {
            public void onClick(View v) {
                onGpsSettingsClick(v);
            }
        };

        CheckBox turnOff = (CheckBox) m_gpsSettings.findViewById(R.id.checkBoxOff);
        CheckBox turnOn = (CheckBox) m_gpsSettings.findViewById(R.id.checkBoxOn);
        turnOff.setOnClickListener(gpsClickListener);
        turnOn.setOnClickListener(gpsClickListener);

        String turnState = "";
        if (jsonGps.has("TURN")) {
            try {
                turnState = jsonGps.getString("TURN");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        turnOff.setChecked(turnState.compareTo("OFF") == 0);
        turnOn.setChecked(turnState.compareTo("ON") == 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_undo:
                onUndo();
                break;
            case R.id.action_next:
                onNext();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private String getTaskTitle() {
        final EditText editTitle = (EditText)findViewById(R.id.editTextTitle);
        String title = editTitle.getText().toString().trim();

        if (title.isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.warning).setMessage(R.string.empty_title).setCancelable(false)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            editTitle.requestFocus();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
            title = "";
        }
        return title;
    }

    private String getSsid() {
        final EditText editSsid = (EditText)m_wifiSettings2.findViewById(R.id.editSSID);
        String ssid = editSsid.getText().toString().trim();

        if (ssid.isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.warning).setMessage(R.string.ssid_required).setCancelable(false)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            editSsid.requestFocus();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
            ssid = "";
        }
        return ssid;
    }

    private String getPassword() {
        RadioButton radioOpen = (RadioButton) m_wifiSettings2.findViewById(R.id.radioOpen);
        final EditText editPassword = (EditText)m_wifiSettings2.findViewById(R.id.editPassword);
        String password = editPassword.getText().toString();

        if (password.isEmpty() && !radioOpen.isChecked()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            RadioButton radioWep = (RadioButton) m_wifiSettings2.findViewById(R.id.radioWep);
            RadioButton radioWpa = (RadioButton) m_wifiSettings2.findViewById(R.id.radioWpa);
            int typeRes = 0;
            if (radioWep.isChecked())
                typeRes = R.string.wep_network;
            if (radioWpa.isChecked())
                typeRes = R.string.wpa_network;

            builder.setTitle(R.string.warning).setMessage(String.format(getString(R.string.password_required), getResources().getString(typeRes))).setCancelable(false)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            editPassword.requestFocus();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
            password = "";
        }
        return password;
    }

    private void showErrorOccurredRetry() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.error).setMessage(R.string.error_occurred_retry).setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private JSONObject getWifiSettings() {
        JSONObject json = new JSONObject();
        CheckBox checkOn = (CheckBox) m_wifiSettings.findViewById(R.id.checkBoxOn);
        CheckBox checkOff = (CheckBox) m_wifiSettings.findViewById(R.id.checkBoxOff);

        if (checkOn.isChecked() || checkOff.isChecked()) {
            try {
                if (checkOff.isChecked())
                    json.put("TURN", "OFF");
                else
                    json.put("TURN", "ON");
            } catch (JSONException e) {
                showErrorOccurredRetry();
                json = new JSONObject();
                e.printStackTrace();
            }
        }

        if (json.has("TURN"))
        {
            CheckBox checkConnect = (CheckBox) m_wifiSettings2.findViewById(R.id.checkConnect);
            if (checkConnect.isChecked()) {
                String ssid = getSsid();

                if (ssid.isEmpty()) {
                    try {
                        json.put("ERROR", true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    RadioButton radioOpen = (RadioButton) m_wifiSettings2.findViewById(R.id.radioOpen);
                    String password = getPassword();

                    if (password.isEmpty() && !radioOpen.isChecked()) {
                        try {
                            json.put("ERROR", true);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        try {
                            JSONObject jsonConnect = new JSONObject();
                            jsonConnect.put("SSID", ssid);
                            if (radioOpen.isChecked()) {
                                jsonConnect.put("TYPE", "OPEN");
                            }
                            else {
                                RadioButton radioWep = (RadioButton) m_wifiSettings2.findViewById(R.id.radioWep);
                                if (radioWep.isChecked()) {
                                    jsonConnect.put("TYPE", "WEP");
                                }
                                else {
                                    RadioButton radioWpa = (RadioButton) m_wifiSettings2.findViewById(R.id.radioWpa);
                                    if (radioWpa.isChecked())
                                        jsonConnect.put("TYPE", "WPA");
                                }
                                jsonConnect.put("PASSWORD", password);
                            }
                            json.put("CONNECT", jsonConnect);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        return json;
    }

    private JSONObject getBluetoothSettings() {
        JSONObject json = new JSONObject();
        CheckBox checkOn = (CheckBox) m_bluetoothSettings.findViewById(R.id.checkBoxOn);
        CheckBox checkOff = (CheckBox) m_bluetoothSettings.findViewById(R.id.checkBoxOff);

        if (checkOn.isChecked() || checkOff.isChecked()) {
            try {
                if (checkOff.isChecked())
                    json.put("TURN", "OFF");
                else
                    json.put("TURN", "ON");
            } catch (JSONException e) {
                showErrorOccurredRetry();
                json = new JSONObject();
                e.printStackTrace();
            }
        }
        return json;
    }

    private JSONObject getGpsSettings() {
        JSONObject json = new JSONObject();
        CheckBox checkOn = (CheckBox) m_gpsSettings.findViewById(R.id.checkBoxOn);
        CheckBox checkOff = (CheckBox) m_gpsSettings.findViewById(R.id.checkBoxOff);

        if (checkOn.isChecked() || checkOff.isChecked()) {
            try {
                if (checkOff.isChecked())
                    json.put("TURN", "OFF");
                else
                    json.put("TURN", "ON");
            } catch (JSONException e) {
                showErrorOccurredRetry();
                json = new JSONObject();
                e.printStackTrace();
            }
        }
        return json;
    }

    private void onUndo() {
        onBackPressed();
        finish();
    }

    private void onNext() {
        String title = getTaskTitle();
        if (title.isEmpty())
            return;

        JSONObject jsonSettings = new JSONObject();

        JSONObject jsonWifi = getWifiSettings();
        if (jsonWifi.has("ERROR"))
            return;

        JSONObject jsonBluetooth = getBluetoothSettings();
        JSONObject jsonGps = getGpsSettings();

        if (jsonWifi.length() == 0 && jsonBluetooth.length() == 0 && jsonGps.length() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.warning).setMessage(R.string.empty_task).setCancelable(false)
            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
            return;
        }

        try {
            jsonSettings.put("TITLE", title);
            if (jsonWifi.length() > 0)
                jsonSettings.put("WIFI", jsonWifi);
            if (jsonBluetooth.length() > 0)
                jsonSettings.put("BLUETOOTH", jsonBluetooth);
            if (jsonGps.length() > 0)
                jsonSettings.put("GPS", jsonGps);
        } catch (JSONException e) {
            showErrorOccurredRetry();
            e.printStackTrace();
            return;
        }

        launchConfirmTaskActivity(jsonSettings);
    }

    private void onWifiSettingsClick(View v) {
        switch (v.getId()) {
            case R.id.checkBoxOff:
                CheckBox checkBoxOff = (CheckBox)v;
                if(checkBoxOff.isChecked()){
                    CheckBox checkConnect = (CheckBox)m_wifiSettings2.findViewById(R.id.checkConnect);
                    CheckBox checkBoxOn = (CheckBox)m_wifiSettings.findViewById(R.id.checkBoxOn);
                    checkBoxOn.setChecked(false);
                    checkConnect.setChecked(false);
                    onWifiSettingsClick(checkConnect);
                }
                break;
            case R.id.checkBoxOn:
                CheckBox checkBoxOn = (CheckBox)v;
                if(checkBoxOn.isChecked()){
                    checkBoxOff = (CheckBox) m_wifiSettings.findViewById(R.id.checkBoxOff);
                    checkBoxOff.setChecked(false);
                }
                else
                {
                    CheckBox checkConnect = (CheckBox)m_wifiSettings2.findViewById(R.id.checkConnect);
                    checkConnect.setChecked(false);
                    onWifiSettingsClick(checkConnect);
                }
                break;
            case R.id.checkConnect:
                CheckBox checkConnect = (CheckBox)v;
                LinearLayout connectOptions = (LinearLayout) m_wifiSettings2.findViewById(R.id.connect_options);
                EditText editSsid = (EditText) m_wifiSettings2.findViewById(R.id.editSSID);
                EditText editPassword = (EditText) m_wifiSettings2.findViewById(R.id.editPassword);
                RadioButton radioOpen = (RadioButton) m_wifiSettings2.findViewById(R.id.radioOpen);
                RadioButton radioWep = (RadioButton) m_wifiSettings2.findViewById(R.id.radioWep);
                RadioButton radioWpa = (RadioButton) m_wifiSettings2.findViewById(R.id.radioWpa);
                if(checkConnect.isChecked()) {
                    checkBoxOff = (CheckBox) m_wifiSettings.findViewById(R.id.checkBoxOff);
                    checkBoxOn = (CheckBox) m_wifiSettings.findViewById(R.id.checkBoxOn);

                    checkBoxOff.setChecked(false);
                    checkBoxOn.setChecked(true);
                }
                editSsid.setEnabled(checkConnect.isChecked());
                editPassword.setEnabled(checkConnect.isChecked());
                radioOpen.setEnabled(checkConnect.isChecked());
                radioWep.setEnabled(checkConnect.isChecked());
                radioWpa.setEnabled(checkConnect.isChecked());
                connectOptions.setVisibility(checkConnect.isChecked() ? View.VISIBLE : View.GONE);
                break;
            case R.id.radioOpen:
                radioOpen = (RadioButton) v;
                TextInputLayout layoutPassword = (TextInputLayout) m_wifiSettings2.findViewById(R.id.layoutPassword);
                if (radioOpen.isChecked())
                    layoutPassword.setVisibility(View.GONE);
                break;
            case R.id.radioWep:
                radioWep = (RadioButton) v;
                layoutPassword = (TextInputLayout) m_wifiSettings2.findViewById(R.id.layoutPassword);
                if (radioWep.isChecked())
                    layoutPassword.setVisibility(View.VISIBLE);
                break;
            case R.id.radioWpa:
                radioWpa = (RadioButton) v;
                layoutPassword = (TextInputLayout) m_wifiSettings2.findViewById(R.id.layoutPassword);
                if (radioWpa.isChecked())
                    layoutPassword.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void onBluetoothSettingsClick(View v) {
        switch (v.getId()) {
            case R.id.checkBoxOff:
                CheckBox checkBoxOff = (CheckBox)v;
                if(checkBoxOff.isChecked()){
                    CheckBox checkBoxOn = (CheckBox)m_bluetoothSettings.findViewById(R.id.checkBoxOn);
                    checkBoxOn.setChecked(false);
                }
                break;
            case R.id.checkBoxOn:
                CheckBox checkBoxOn = (CheckBox)v;
                if(checkBoxOn.isChecked()){
                    checkBoxOff = (CheckBox) m_bluetoothSettings.findViewById(R.id.checkBoxOff);
                    checkBoxOff.setChecked(false);
                }
                break;
        }
    }

    private void onGpsSettingsClick(View v) {
        switch (v.getId()) {
            case R.id.checkBoxOff:
                CheckBox checkBoxOff = (CheckBox)v;
                if(checkBoxOff.isChecked()){
                    CheckBox checkBoxOn = (CheckBox)m_gpsSettings.findViewById(R.id.checkBoxOn);
                    checkBoxOn.setChecked(false);
                }
                break;
            case R.id.checkBoxOn:
                CheckBox checkBoxOn = (CheckBox)v;
                if(checkBoxOn.isChecked()){
                    checkBoxOff = (CheckBox) m_gpsSettings.findViewById(R.id.checkBoxOff);
                    checkBoxOff.setChecked(false);
                }
                break;
        }
    }


    private void launchConfirmTaskActivity(JSONObject jsonSettings) {
        Intent intent = new Intent(this, InfoTaskActivity.class);
        intent.putExtra("TASK", jsonSettings.toString());
        intent.putExtra("CONFIRM", true);
        startActivityForResult(intent, CONFIRM_TASK_ACTIVITY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case CONFIRM_TASK_ACTIVITY_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    Intent intent = new Intent(data);
                    if (m_taskId != -1)
                        intent.putExtra("TASK_ID", m_taskId);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
        }
    }
}
