package com.example.esame1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public abstract class SwipeHelper extends ItemTouchHelper.SimpleCallback {
    private RecyclerView recyclerView;
    private List<UnderlayButton> buttons;
    private GestureDetector gestureDetector;
    private int swipedPos = -1;
    private float swipeThreshold = 100;
    private Map<Integer, List<UnderlayButton>> leftButtonsBuffer;
    private Map<Integer, List<UnderlayButton>> rightButtonsBuffer;
    private Queue<Integer> recoverQueue;

    private GestureDetector.SimpleOnGestureListener gestureListener = new GestureDetector.SimpleOnGestureListener(){
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            for (UnderlayButton button : buttons){
                if(button.onClick(e.getX(), e.getY()))
                    break;
            }

            return true;
        }
    };

    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent e) {
            if (swipedPos < 0)
                return false;
            Point point = new Point((int) e.getRawX(), (int) e.getRawY());

            RecyclerView.ViewHolder swipedViewHolder = recyclerView.findViewHolderForAdapterPosition(swipedPos);
            View swipedItem = swipedViewHolder.itemView;
            Rect rect = new Rect();
            swipedItem.getGlobalVisibleRect(rect);

            if (e.getAction() == MotionEvent.ACTION_DOWN || e.getAction() == MotionEvent.ACTION_UP ||e.getAction() == MotionEvent.ACTION_MOVE) {
                if (rect.top < point.y && rect.bottom > point.y)
                    gestureDetector.onTouchEvent(e);
                else {
                    recoverQueue.add(swipedPos);
                    swipedPos = -1;
                    recoverSwipedItem();
                }
            }
            return false;
        }
    };

    public SwipeHelper(Context context, RecyclerView recyclerView) {
        super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        this.recyclerView = recyclerView;
        this.buttons = new ArrayList<>();
        this.gestureDetector = new GestureDetector(context, gestureListener);
        this.recyclerView.setOnTouchListener(onTouchListener);
        leftButtonsBuffer = new HashMap<>();
        rightButtonsBuffer = new HashMap<>();
        recoverQueue = new LinkedList<Integer>(){
            @Override
            public boolean add(Integer o) {
                if (contains(o))
                    return false;
                else
                    return super.add(o);
            }
        };

        attachSwipe();
    }


    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        int itemPos = viewHolder.getAdapterPosition();

        if (swipedPos != itemPos)
            recoverQueue.add(swipedPos);

        swipedPos = itemPos;

        if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
            if (direction == ItemTouchHelper.LEFT) {
                if (rightButtonsBuffer.containsKey(swipedPos))
                    buttons = rightButtonsBuffer.get(swipedPos);
                else
                    buttons.clear();
                rightButtonsBuffer.clear();
            } else {
                if (leftButtonsBuffer.containsKey(swipedPos))
                    buttons = leftButtonsBuffer.get(swipedPos);
                else
                    buttons.clear();
                leftButtonsBuffer.clear();
            }
        }

        swipeThreshold = viewHolder.itemView.getWidth() / 6;
        recoverSwipedItem();
    }

    @Override
    public float getSwipeThreshold(RecyclerView.ViewHolder viewHolder) {
        return swipeThreshold;
    }

    @Override
    public float getSwipeEscapeVelocity(float defaultValue) {
        return 0.1f * defaultValue;
    }

    @Override
    public float getSwipeVelocityThreshold(float defaultValue) {
        return 5.0f * defaultValue;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        int itemPos = viewHolder.getAdapterPosition();
        float translationX = dX;
        View itemView = viewHolder.itemView;

        if (itemPos < 0){
            swipedPos = itemPos;
            return;
        }

        if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE && dX != 0) {
            List<UnderlayButton> buffer = new ArrayList<>();
            if(dX < 0) {
                if (!rightButtonsBuffer.containsKey(itemPos)){
                    instantiateUnderlayButton(viewHolder, new ArrayList<UnderlayButton>(), buffer);
                    rightButtonsBuffer.put(itemPos, buffer);
                }
                else {
                    buffer = rightButtonsBuffer.get(itemPos);
                }
            } else {
                if (!leftButtonsBuffer.containsKey(itemPos)){
                    instantiateUnderlayButton(viewHolder, buffer, new ArrayList<UnderlayButton>());
                    leftButtonsBuffer.put(itemPos, buffer);
                }
                else {
                    buffer = leftButtonsBuffer.get(itemPos);
                }
            }

            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) itemView.getLayoutParams();
            translationX = dX * buffer.size() * (itemView.getHeight() *2/3 + lp.leftMargin) / itemView.getWidth();
            drawButtons(c, itemView, buffer, itemPos, translationX);
        }

        super.onChildDraw(c, recyclerView, viewHolder, translationX, dY, actionState, isCurrentlyActive);
    }

    private synchronized void recoverSwipedItem(){
        while (!recoverQueue.isEmpty()){
            int pos = recoverQueue.poll();
            if (pos > -1) {
                recyclerView.getAdapter().notifyItemChanged(pos);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void drawButtons(Canvas context, View itemView, List<UnderlayButton> buffer, int itemPos, float dX) {
        float right = itemView.getRight();
        float left = itemView.getLeft();
        float buttonWidth = Math.abs(dX / buffer.size());
        float buttonPaddingHor = buttonWidth / 5;
        float buttonPaddingVer = (itemView.getHeight() - (buttonWidth - buttonPaddingHor * 2)) / 2;
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) itemView.getLayoutParams();
        int cornerRadius = lp.leftMargin;
        RectF buttonRect = new RectF();
        Rect iconRect = new Rect();

        if (dX != 0) {
            for (UnderlayButton button : buffer) {
                if (dX < 0) {
                    left = right - buttonWidth + cornerRadius;
                    buttonRect = new RectF(left, itemView.getTop(), right, itemView.getBottom());
                    right = left - cornerRadius;
                }
                else {
                    right = left + buttonWidth - cornerRadius;
                    buttonRect = new RectF(left, itemView.getTop(), right, itemView.getBottom());
                    left = right + cornerRadius;
                }

                RectF iconRectF = new RectF(buttonRect);
                iconRectF.inset(buttonPaddingHor, buttonPaddingVer);
                iconRectF.roundOut(iconRect);
                button.onDraw(context, buttonRect, iconRect, cornerRadius, itemPos);
            }
        }
    }

    public void attachSwipe(){
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(this);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    public abstract void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> leftUnderlayButtons, List<UnderlayButton> rightUnderlayButtons);

    public static class UnderlayButton {
        private Context m_context;
        private int m_drawable;
        private int m_bkgColor;
        private int m_color;
        private int m_itemPos;
        private RectF m_clickRegion;
        private UnderlayButtonClickListener m_clickListener;

        public UnderlayButton(Context context, int drawable, int bkgColor, int color, UnderlayButtonClickListener clickListener) {
            m_context = context;
            m_drawable = drawable;
            m_bkgColor = bkgColor;
            m_color = color;
            m_clickListener = clickListener;
        }

        public boolean onClick(float x, float y) {
            if (m_clickRegion != null && m_clickRegion.contains(x, y)){
                m_clickListener.onClick(m_itemPos);
                return true;
            }

            return false;
        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        public void onDraw(Canvas c, RectF buttonRect, Rect iconRect, int cornerRadius, int itemPos) {
            Paint p = new Paint();
            p.setColor(m_context.getColor(R.color.colorSurface));
            c.drawRoundRect(buttonRect, cornerRadius, cornerRadius, p);
            p.setColor(m_context.getColor(m_bkgColor));
            c.drawRoundRect(buttonRect, cornerRadius, cornerRadius, p);
            Drawable deleteIcon = m_context.getDrawable(m_drawable);
            deleteIcon.setBounds(iconRect);
            deleteIcon.setTint(m_context.getColor(m_color));
            deleteIcon.draw(c);

            m_clickRegion = buttonRect;
            this.m_itemPos = itemPos;
        }
    }

    public interface UnderlayButtonClickListener {
        void onClick(int pos);
    }
}
