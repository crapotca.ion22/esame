package com.example.esame1;

import android.content.Context;
import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

public class TaskItem {
    public int DIRTY_TASK_LENGTH = 256;
    private final String URL_PARAM = "?task=";
    private String m_dirtyTask;
    private String m_cleanTask;
    private Boolean m_isValid = false;
    private long m_id;
    private Context m_context;

    public TaskItem(Context context) {
        m_context = context;
    }

    public TaskItem(Context context, String task, Boolean isDirty) {
        m_context = context;
        if (!isDirty)
        {
            m_isValid = true;
            setTaskFromClean(task);
        }
        else
        {
            m_isValid = isValidTaskQrCode(task);
            if (isValid())
                setTaskFromDirty(task);
        }
    }

    @Override
    public String toString() {
        String item = String.format("ID: %d\nTASK: %s\nEXTERNAL TASK: %s", getId(), getCleanTask(), getDirtyTask());
        return item;
    }

    public long getId() {
        return m_id;
    }

    public void setId(long id) {
        m_id = id;
    }

    private String encodeTask(String cleanTask) {
        String encodedTask = "";
        byte[] data = cleanTask.getBytes(StandardCharsets.UTF_8);
        encodedTask = Base64.encodeToString(data, Base64.DEFAULT);
        return encodedTask;
    }

    private String decodeTask(String dirtyTask) {
        String decodedTask = "";
        byte[] data = Base64.decode(dirtyTask, Base64.DEFAULT);
        decodedTask = new String(data, StandardCharsets.UTF_8);
        return decodedTask;
    }

    private String addPaddingToTask(String dirtyTask) {
        while (dirtyTask.length() != DIRTY_TASK_LENGTH) {
            for (int x = dirtyTask.length(); x < DIRTY_TASK_LENGTH; x++) {
                dirtyTask = dirtyTask.concat(" ");
            }
            if (dirtyTask.length() > DIRTY_TASK_LENGTH)
                DIRTY_TASK_LENGTH *= 1.1;
        }
        return dirtyTask;
    }

    private String removePaddingFromTask(String dirtyTask) {
        return dirtyTask.trim();
    }

    public void setTaskFromClean(String cleanTask) {
        m_cleanTask = cleanTask;
        String apkUrl = m_context.getResources().getString(R.string.APK_URL).concat(URL_PARAM);

        String dirtyTask = "";
        try {
            dirtyTask = apkUrl.concat(URLEncoder.encode(encodeTask(cleanTask), StandardCharsets.UTF_8.name()));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        m_dirtyTask = addPaddingToTask(dirtyTask);
    }


    public void setTaskFromDirty(String dirtyTask) {
        m_dirtyTask = removePaddingFromTask(dirtyTask);
        String apkUrl = m_context.getResources().getString(R.string.APK_URL).concat(URL_PARAM);
        dirtyTask = getDirtyTask().replace(apkUrl, "");

        String cleanTask = "";
        try {
            dirtyTask = URLDecoder.decode(dirtyTask, StandardCharsets.UTF_8.name());
            cleanTask = decodeTask(dirtyTask);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        m_cleanTask = cleanTask;
    }

    public String getCleanTask() { return m_cleanTask; }
    public String getDirtyTask() { return m_dirtyTask; }

    public String getTitle() throws JSONException {
        String title = "";
        JSONObject task = new JSONObject(getCleanTask());
        title = task.getString("TITLE");
        return title;
    }

    public void setTitle(String title) throws JSONException {
        JSONObject task = new JSONObject(getCleanTask());
        task.put("TITLE", title);
        setTaskFromClean(task.toString());
    }

    public boolean hasWifiSettings() throws JSONException {
        boolean has = false;
        JSONObject task = new JSONObject(getCleanTask());
        has = task.has("WIFI");
        return has;
    }

    public boolean hasBluetoothSettings() throws JSONException {
        boolean has = false;
        JSONObject task = new JSONObject(getCleanTask());
        has = task.has("BLUETOOTH");
        return has;
    }

    public boolean hasGpsSettings() throws JSONException {
        boolean has = false;
        JSONObject task = new JSONObject(getCleanTask());
        has = task.has("GPS");
        return has;
    }

    public Boolean isValid() {
        return m_isValid;
    }

    private Boolean isValidTaskQrCode(String dirtyTask) {
        return dirtyTask.startsWith(m_context.getResources().getString(R.string.APK_URL).concat(URL_PARAM));
    }
}
