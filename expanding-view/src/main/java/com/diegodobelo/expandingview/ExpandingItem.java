/**
 * Copyright (c) 2016, Diego Bezerra <diego.bezerra@gmail.com>
 * Permission to use, copy, modify, and/or distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright notice
 * and this permission notice appear in all copies.
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT,
 * OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 * ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 **/
package com.diegodobelo.expandingview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import utils.CustomViewUtils;

/**
 * Created by diego on 5/5/16.
 */
public class ExpandingItem extends RelativeLayout {
    /**
     * Constant defining default animation duration in milliseconds.
     */
    private static final int DEFAULT_ANIM_DURATION = 300;

    /**
     * Member variable to hold the Item Layout. Set by item_layout in ExpandingItem layout.
     */
    private ViewGroup mItemLayout;

    /**
     * The layout inflater.
     */
    private LayoutInflater mInflater;

    /**
     * Member variable to hold the base layout. Should not be changed.
     */
    private RelativeLayout mBaseLayout;

    /**
     * Member variable to hold sub items. Should not be changed.
     */
    private LinearLayout mBaseSubListLayout;

    /**
     * Member variable to hold the indicator icon.
     * Can be set by {@link #setIndicatorIcon(int, int}.
     */
    private ImageView mIndicatorImage;

    private TextView mTitle;

    /**
     * Member variable to hold the expandable part of indicator. Should not be changed.
     */
    private View mIndicatorBackground;

    /**
     * Member variable to hold the indicator container. Should not be changed.
     */
    private ViewGroup mIndicatorContainer;

    /**
     * Member variable to hold the measured item height.
     */
    private int mItemHeight;

    /**
     * Member variable to hold the measured sub item height.
     */
    private int mSubItemsTotalHeight;

    /**
     * Member variable to hold the measured sub item width.
     */
    private int mSubItemWidth;

    /**
     * Member variable to hold the sub items count.
     */
    private int mSubItemCount;

    /**
     * Member variable to hold the animation duration.
     * Set by animation_duration in ExpandingItem layout in milliseconds.
     * Default is 300ms.
     */
    private int mAnimationDuration;

    /**
     * Member variable to hold the boolean value that defines if the animation should be shown.
     * Set by show_animation in ExpandingItem layout. Default is true.
     */
    private boolean mShowAnimation;

    /**
     * Member variable to hold the boolean value that defines if the sub list will start collapsed or not.
     * Set by start_collapsed in ExpandingItem layout. Default is true.
     */
    private boolean mStartCollapsed;

    /**
     * Member variable to hold the state of sub items. true if shown. false otherwise.
     */
    private boolean mSubItemsShown;

    /**
     * Member variable to hold the layout resource of items. Set by item_layout in ExpandingItem layout.
     */
    private int mItemLayoutId;

    /**
     * Holds a reference to the parent. Used to calculate positioning.
     */
    private ExpandingList mParent;

    /**
     * Member variable to hold the listener of item state change.
     */
    private OnItemStateChanged mListener;

    /**
     * Constructor.
     * @param context
     * @param attrs
     */
    public ExpandingItem(Context context, AttributeSet attrs) {
        super(context, attrs);

        mSubItemsTotalHeight = 0;
        readAttributes(context, attrs);
        setupStateVariables();
        inflateLayouts(context);

        addView(mBaseLayout);
    }

    /**
     * Setup the variables that defines item state.
     */
    private void setupStateVariables() {
        if (!mShowAnimation) {
            mAnimationDuration = 0;
        }
    }

    /**
     * Read all custom styleable attributes.
     * @param context The custom View Context.
     * @param attrs The attributes to be read.
     */
    private void readAttributes(Context context, AttributeSet attrs) {
        TypedArray array = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.ExpandingItem, 0, 0);

        try {
            mItemLayoutId = array.getResourceId(R.styleable.ExpandingItem_item_layout, 0);
            mShowAnimation = array.getBoolean(R.styleable.ExpandingItem_show_animation, true);
            mStartCollapsed = array.getBoolean(R.styleable.ExpandingItem_start_collapsed, true);
            mAnimationDuration = array.getInt(R.styleable.ExpandingItem_animation_duration, DEFAULT_ANIM_DURATION);
        } finally {
            array.recycle();
        }
    }

    /**
     * Method to inflate all layouts.
     * @param context The custom View Context.
     */
    private void inflateLayouts(Context context) {
        mInflater = LayoutInflater.from(context);
        mBaseLayout = (RelativeLayout) mInflater.inflate(R.layout.expanding_item_base_layout, null, false);
        mBaseSubListLayout = mBaseLayout.findViewById(R.id.base_sub_list_layout);
        mIndicatorImage = mBaseLayout.findViewById(R.id.indicator_image);
        mTitle = mBaseLayout.findViewById(R.id.title);
        mIndicatorBackground = mBaseLayout.findViewById(R.id.indicator_circle);
        mIndicatorContainer = mBaseLayout.findViewById(R.id.indicator_container);
        mIndicatorContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleExpanded();
            }
        });

        if (mItemLayoutId != 0) {
            mItemLayout = (ViewGroup) mInflater.inflate(mItemLayoutId, mBaseLayout, false);
        }
    }

    /**
     * Collapses the sub items.
     */
    public void collapse() {
        mSubItemsShown = false;
        mBaseSubListLayout.post(new Runnable() {
            @Override
            public void run() {
                CustomViewUtils.setViewHeight(mBaseSubListLayout, 0);
            }
        });

        fixCollapseIcon();
    }

    private void fixCollapseIcon() {
        ImageView collapseIcon = mBaseLayout.findViewById(R.id.collapse_icon);
        if (mSubItemCount == 0) {
            collapseIcon.setImageDrawable(null);
        } else {
            if (!mSubItemsShown) {
                collapseIcon.setImageResource(R.drawable.ic_down_chevron);
            } else {
                collapseIcon.setImageResource(R.drawable.ic_up_chevron);
            }
        }
    }

    /**
     * Expand or collapse the sub items.
     */
    public void toggleExpanded() {
        if (mSubItemCount == 0)
            return;

        toggleSubItems();
        expandSubItemsWithAnimation(0f);
        animateSubItemsIn();
        fixCollapseIcon();
    }

    /**
     * Method to adjust Item position in parent if its sub items are outside screen.
     */
    private void adjustItemPosIfHidden() {
        int parentHeight = mParent.getMeasuredHeight();
        int[] parentPos = new int[2];
        mParent.getLocationOnScreen(parentPos);
        int parentY = parentPos[1];
        int[] itemPos = new int[2];
        mBaseLayout.getLocationOnScreen(itemPos);
        int itemY = itemPos[1];

        int endPosition = itemY + mItemHeight + mSubItemsTotalHeight;
        int parentEnd = parentY + parentHeight;
        if (endPosition > parentEnd) {
            int delta = endPosition - parentEnd;
            int itemDeltaToTop = itemY - parentY;
            if (delta > itemDeltaToTop) {
                delta = itemDeltaToTop;
            }
            mParent.scrollUpByDelta(delta);
        }

        CustomViewUtils.setViewMargin(mBaseSubListLayout, 0, 0, 0, 0);
    }

    public void setClickListener() {
        mBaseLayout.findViewById(R.id.item_container).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleExpanded();
            }
        });
    }

    public void setTitle(String title) {
        mTitle.setText(title);
    }

    public void setStartCollapsed(Boolean startCollapsed) { mStartCollapsed = startCollapsed; }

    public void setIndicatorBackground(int bkgColor) {
        ((GradientDrawable) mIndicatorBackground.getBackground().mutate()).setColor(ContextCompat.getColor(getContext(), bkgColor));
    }

    /**
     * Set the indicator icon by resource.
     * @param iconRes The icon resource.
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void setIndicatorIcon(int iconRes, int iconColor) {
        Drawable icon = ContextCompat.getDrawable(getContext(), iconRes);
        icon.setTint(ContextCompat.getColor(getContext(), iconColor));
        mIndicatorImage.setImageDrawable(icon);
    }

    /**
     * Creates a sub item based on sub_item_layout Layout, set as ExpandingItem layout attribute.
     * @return The inflated sub item view.
     */
    @Nullable
    public View createSubItem(int subItemLayoutRes) {
        final ViewGroup subItemLayout = (ViewGroup) mInflater.inflate(subItemLayoutRes, mBaseSubListLayout, false);
        mBaseSubListLayout.addView(subItemLayout);
        mSubItemCount++;
        setSubItemDimensions(subItemLayout);

        if (mBaseSubListLayout.getChildCount() == 1) {
            if (mStartCollapsed) {
                collapse();
            } else {
                subItemLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        toggleExpanded();
                    }
                });
            }
        }

        return subItemLayout;
    }

    /**
     * Get a sub item at the given position.
     * @param position The sub item position. Should be > 0.
     * @return The sub item inflated view at the given position.
     */
    public View getSubItemView(int position) {
        if (mBaseSubListLayout.getChildAt(position) != null) {
            return mBaseSubListLayout.getChildAt(position);
        }
        throw new RuntimeException("There is no sub item for position " + position +
                ". There are only " + mBaseSubListLayout.getChildCount() + " in the list.");
    }

    /**
     * Set the parent in order to auto scroll.
     * @param parent The parent of type {@link ExpandingList}
     */
    protected void setParent(ExpandingList parent) {
        mParent = parent;
    }

    /**
     * Measure sub items dimension.
     * @param v The sub item to measure.
     */
    private void setSubItemDimensions(final ViewGroup v) {
        v.post(new Runnable() {
            @Override
            public void run() {
                mSubItemsTotalHeight += v.getMeasuredHeight();
                mSubItemWidth = v.getMeasuredWidth();
            }
        });
    }

    /**
     * Toggle sub items collapsed/expanded
     */
    private void toggleSubItems() {
        mSubItemsShown = !mSubItemsShown;
        if (mListener != null) {
            mListener.itemCollapseStateChanged(mSubItemsShown);
        }
    }

    /**
     * Show sub items animation.
     */
    private void animateSubItemsIn() {
        for (int i = 0; i < mSubItemCount; i++) {
            animateSubViews((ViewGroup) mBaseSubListLayout.getChildAt(i), i);
            animateViewAlpha((ViewGroup) mBaseSubListLayout.getChildAt(i), i);
        }
    }

    /**
     * Show sub items translation animation.
     * @param viewGroup The sub item to animate
     * @param index The sub item index. Needed to calculate delays.
     */
    private void animateSubViews(final ViewGroup viewGroup, int index) {
        if (viewGroup == null) {
            return;
        }
        viewGroup.setLayerType(ViewGroup.LAYER_TYPE_HARDWARE, null);
        ValueAnimator animation = mSubItemsShown ?
                ValueAnimator.ofFloat(0f, 1f) :
                ValueAnimator.ofFloat(1f, 0f);
        animation.setDuration(mAnimationDuration);
        int delay = index * mAnimationDuration / mSubItemCount;
        int invertedDelay = (mSubItemCount - index) * mAnimationDuration / mSubItemCount;
        animation.setStartDelay(mSubItemsShown ? delay / 2 : invertedDelay / 2);

        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float val = (float) animation.getAnimatedValue();
                viewGroup.setX((mSubItemWidth / 2 * val) - mSubItemWidth / 2);
            }
        });

        animation.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                viewGroup.setLayerType(ViewGroup.LAYER_TYPE_NONE, null);
            }
        });

        animation.start();
    }

    /**
     * Show sub items alpha animation.
     * @param viewGroup The sub item to animate
     * @param index The sub item index. Needed to calculate delays.
     */
    private void animateViewAlpha(final ViewGroup viewGroup, int index) {
        if (viewGroup == null) {
            return;
        }
        ValueAnimator animation = mSubItemsShown ?
                ValueAnimator.ofFloat(0f, 1f) :
                ValueAnimator.ofFloat(1f, 0f);
        animation.setDuration(mSubItemsShown ? mAnimationDuration * 2 : mAnimationDuration);
        int delay = index * mAnimationDuration / mSubItemCount;
        animation.setStartDelay(mSubItemsShown ? delay / 2 : 0);

        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float val = (float) animation.getAnimatedValue();
                viewGroup.setAlpha(val);
            }
        });

        animation.start();
    }

    /**
     * Expand the sub items container with animation
     * @param startingPos The position from where the animation should start. Useful when removing sub items.
     */
    private void expandSubItemsWithAnimation(float startingPos) {
        if (mBaseSubListLayout != null) {
            final int totalHeight = mSubItemsTotalHeight;
            ValueAnimator animation = mSubItemsShown ?
                    ValueAnimator.ofFloat(startingPos, totalHeight) :
                    ValueAnimator.ofFloat(totalHeight, startingPos);
            animation.setInterpolator(new AccelerateDecelerateInterpolator());
            animation.setDuration(mAnimationDuration);

            animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float val = (float) animation.getAnimatedValue();
                    CustomViewUtils.setViewHeight(mBaseSubListLayout, (int) val);
                }
            });

            animation.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    if (mSubItemsShown) {
                        adjustItemPosIfHidden();
                        CustomViewUtils.setViewHeight(mBaseSubListLayout, -1);
                    }
                }
            });

            animation.start();
        }
    }

    /**
     * Interface to notify item state changed.
     */
    public interface OnItemStateChanged {
        /**
         * Notify if item was expanded or collapsed.
         * @param expanded true if expanded. false otherwise.
         */
        void itemCollapseStateChanged(boolean expanded);
    }

}
