package com.notbytes.barcode_reader;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;

import static java.lang.Math.min;

public class ScannerOverlay extends ViewGroup {
    private float left, top, scanLineTop, scanLineLeft, scanCornerLengthRatio;
    private int rectWidth, rectHeight, scanLineLength, scanLineThickness, scanLineColor, scanCornerColor, scanCornerThickness, scanLineSpeed;
    private boolean revAnimation;

    public ScannerOverlay(Context context) {
        super(context);
    }

    public ScannerOverlay(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScannerOverlay(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ScannerOverlay,
                0, 0);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        rectWidth = (int) (min(w, h) * 0.8);
        rectHeight = rectWidth;
        left = (w - rectWidth) / 2;
        top = (h - rectHeight) / 2;
        scanLineLength = (int) (rectWidth / 0.8 * 0.9);
        scanLineThickness = (int) (rectHeight * 0.025);
        scanLineLeft = (w - scanLineLength) / 2;
        scanLineTop = top;
        scanLineColor = getResources().getColor(R.color.scanner_line);
        scanLineSpeed = getResources().getInteger(R.integer.scan_line_speed);
        scanCornerColor = getResources().getColor(R.color.scanner_corner);
        scanCornerThickness = (int) (rectHeight * 0.03);
        scanCornerLengthRatio = (float) 0.25;

        invalidate();
        requestLayout();
    }

    @Override
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint line = new Paint();
        line.setColor(scanCornerColor);
        line.setStrokeWidth(Float.valueOf(scanCornerThickness));
        // draw transparent rect
        Paint eraser = new Paint();
        eraser.setAntiAlias(true);
        eraser.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        RectF rect = new RectF(left - scanCornerThickness, top - scanCornerThickness, left + rectWidth + scanCornerThickness, top + rectHeight + scanCornerThickness);
        canvas.drawRect(rect, line);
        rect = new RectF(left, top, left + rectWidth, top + rectHeight);
        canvas.drawRect(rect, eraser);
        rect = new RectF(left + rectWidth * scanCornerLengthRatio, top - scanCornerThickness, left + rectWidth - rectWidth * scanCornerLengthRatio, top + rectHeight + scanCornerThickness);
        canvas.drawRect(rect, eraser);
        rect = new RectF(left - scanCornerThickness, top + rectHeight * scanCornerLengthRatio, left + rectWidth + scanCornerThickness, top + rectHeight - rectHeight * scanCornerLengthRatio);
        canvas.drawRect(rect, eraser);

        Drawable icon = getResources().getDrawable(R.drawable.ic_qr_code);
        icon.setBounds((int) left, (int) top, (int) left + rectWidth, (int) top + rectHeight);
        icon.setTint(getResources().getColor(R.color.scanner_qr_code));
        icon.draw(canvas);

        // draw "animated" horizontal line
        if (scanLineTop >= top + rectHeight) {
            revAnimation = true;
        } else if (scanLineTop <= top) {
            revAnimation = false;
        }

        float delta = (float) (scanLineThickness * 0.1 * scanLineSpeed);
        if (revAnimation)
            scanLineTop -= delta;
        else
            scanLineTop += delta;

        line.setStrokeWidth(Float.valueOf(scanLineThickness));
        line.setColor(scanLineColor);
        canvas.drawLine(scanLineLeft, scanLineTop, scanLineLeft + scanLineLength, scanLineTop, line);

        // draw square corners

        /*
        canvas.drawLine(left, top, (float) (left + rectWidth*scanCornerLengthRatio), top, line);
        canvas.drawLine((float) (left + rectWidth - rectWidth*scanCornerLengthRatio), top, left + rectWidth, top, line);
        canvas.drawLine(left, top, left, (float) (top + rectHeight*scanCornerLengthRatio), line);
        canvas.drawLine(left + rectWidth, top, left + rectWidth, (float) (top + rectHeight*scanCornerLengthRatio), line);
        canvas.drawLine(left, top + rectHeight, (float) (left + rectWidth*scanCornerLengthRatio), top + rectHeight, line);
        canvas.drawLine((float) (left + rectWidth - rectWidth*scanCornerLengthRatio), top + rectHeight, left + rectWidth, top + rectHeight, line);
        canvas.drawLine(left, (float) (top + rectHeight - rectHeight*scanCornerLengthRatio), left, top + rectHeight, line);
        canvas.drawLine(left + rectWidth, (float) (top + rectHeight - rectHeight*scanCornerLengthRatio), left + rectWidth, top + rectHeight, line);
        */

        invalidate();
    }
}